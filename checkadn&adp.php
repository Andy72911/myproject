<?php
session_start();
if(isset($_SESSION['log']) ){
  header("location:administrator.php");
  exit();
}

$name = $_POST['name'];
$account = $_POST['account'];
$password = $_POST['password'];

require_once("dbtools.inc.php");
$link = create_connection() or die("connecting fails!");
$sql = " SELECT * FROM users WHERE account = '$account';";
$result = execute_sql($link, 'u0733128', $sql);

$row=mysqli_fetch_assoc($result);

if($row['administrator'] == 'administrator'){   //判斷是否為管理員
  if($name!=$row['name']){
    header("location:interface.html");  //確認姓名是否相符
    exit();
  }
  if($account!=$row['account']){
    header("location:interface.html");  //確認帳號是否相符
    exit();
  }
  if($password!=$row['passwd']){
    header("location:interface.html");  //確認密碼是否相符，若不相符跳回到註冊頁面
    exit();
  }
  header("location:administrator.php");  //是管理員姓名帳密皆相符，跳到管理頁面
  exit();
}else{
  header("location:login.php");          //不是直接跳到一般會員登入頁面
  exit();
}

session_start();
$_SESSION['log'] = 'OK';      //未登入確認
header("location:administrator.php");
exit();
mysqli_close($link);
?>
